# Docker Repository

[![pipeline status](https://gitlab.com/clucky/docker/badges/master/pipeline.svg)](https://gitlab.com/clucky/docker/-/commits/master)

> Build docker images, test and deploy.

## `Stm32-builder`

- `arm-eabi-none-xxx` included.
- Used for `STM32` projects.

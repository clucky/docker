#
# Dockerfile for building AOSP Platform
#
# TODO:
#	Write auto building script.( git fetch/pull and build )
#	Add version meta data.
#
# Commands:
#	build image:
#		docker build -t [ tag ] ./
#		docker build -f aosp.dockerfile \
#		-t [ tag ] \
#		--build-arg KNOWN_HOSTS=${known_hosts_file} \
#		--build-arg DEVADM_PRIVATE_KEY=${ssh_private_key} \
#		./
#	docker run:
#		docker run -it --rm --init \
#		-e HOST_UID=$(id -u) -e HOST_GID=$(id -g) \
#		-v ${WORK_PATH}:/home/stm32/work \
#		-v ${CCACHE_DIR}:/home/stm32/ccache \
#		--name stm32-docker \
#		--hostname stm32-docker \
#		stm32
#
# NOTE:
#	MUST 'docker run' with '--init' option.
#	Use only for building.
#
# Author: Jaeyeong Huh <jay.jyhuh@gmail.com>
#
# Release:
#	v0.01	2018-12-14	jyhuh	Initial version
#	v0.1	2020-03-25	jay		AOSP Initial version

#FROM ubuntu:latest
FROM ubuntu:18.04

MAINTAINER Jaeyeong Huh <jay.jyhuh@gmail.com>

ENV USER_ID stm32
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get upgrade -y \
	&& apt-get clean && apt-get autoremove && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# /bin/sh points to Dash by default, reconfigure to use bash until Android
# build becomes POSIX compliant
RUN echo "dash dash/sh boolean false" | debconf-set-selections \
	&& dpkg-reconfigure -p critical dash

#RUN apt-get update && apt-get install -y --no-install-recommends \
#		apt-utils

RUN apt-get update && apt-get install -y \
	net-tools \
	iproute2 \
	sudo \
	locales tzdata \
	&& locale-gen en_US.UTF-8 \
	&& apt-get clean && apt-get autoremove && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN apt-get update && apt-get install -y \
	git cmake \
	&& apt-get clean && apt-get autoremove && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# GCC toolchain for STM32 ARM Cortex-M
# GDB included.
RUN apt-get update && apt-get install -y \
	software-properties-common \
	&& add-apt-repository -y ppa:team-gcc-arm-embedded/ppa \
	&& apt-get update && apt-get install -y \
	gcc-arm-embedded \
	&& apt-get clean && apt-get autoremove && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# GCC toolchain from Ubuntu for STM32 ARM Cortex-M
# GDB NOT included.
#RUN apt-get update && apt-get install -y \
	#gcc-arm-none-eabi \
	#&& apt-get clean && apt-get autoremove && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN apt-get update && apt-get install -y \
	libusb-1.0.0-dev \
	usbutils \
	&& apt-get clean && apt-get autoremove && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN apt-get update && apt-get install -y \
	curl \
	zip \
	&& apt-get clean && apt-get autoremove && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# We need this because of this https://blog.phusion.nl/2015/01/20/docker-and-the-pid-1-zombie-reaping-problem/
# Here is solution https://engineeringblog.yelp.com/2016/01/dumb-init-an-init-for-docker.html
RUN curl --create-dirs -sSLo /usr/local/bin/dumb-init \
	https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 \
	&& chmod a+x /usr/local/bin/dumb-init

#	Set Timezone
RUN ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime

#	Add user in container as sudoer without password.
RUN useradd -m -u 1000 -U -G adm,sudo ${USER_ID} \
	&& passwd -d ${USER_ID}

ENV DEBIAN_FRONTEND teletype

# args about ssh key is defined from docker build command.
#ARG KNOWN_HOSTS
#ARG DEVADM_PRIVATE_KEY
#COPY ${KNOWN_HOSTS} ${DEVADM_PRIVATE_KEY} /home/${USER_ID}/.ssh/

COPY user-map.sh build.sh /usr/bin/

#CMD [ "/usr/local/bin/dumb-init", "--", "/usr/bin/user-map.sh" ]
CMD [ "/usr/local/bin/dumb-init", "-cv", "/usr/bin/user-map.sh" ]

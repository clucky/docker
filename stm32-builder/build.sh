#!/bin/bash

set -e

SRC_PATH="$HOME/work"
TIMESTAMP=$( date +%s )

export USE_CCACHE=1
export CCACHE_TMPDIR="/tmp"
if [[ -z "${CCACHE_DIR}" ]] ; then
	export CCACHE_DIR="${HOME}/ccache"
fi

help()
{
	echo "Usage: $(basename $0) [ -s SRC_PATH  | -d ]"
	#echo "where  OPTIONS := "
	echo "  -s SRC_PATH     Source top directory"
	echo "  -d              Debug mode"
	echo "  -o OPT          Option argument"
	echo "  -t TYPE         Build type"
	echo "  -v VERSION      Version"
	echo "  -h              Display this help and exit"
	exit 1;
}

OPTSPEC="hds:o:t:v:"
while getopts $OPTSPEC  opt ; do
	case $opt in
		d )
			DEBUG=1
			;;
		s )
			SRC_PATH=$OPTARG
			;;
    o )
      OPTION=${OPTION}"  "${OPTARG}
      ;;
    t )
      BUILD_TYPE=${OPTARG}
      ;;
    v )
      VERSION=${OPTARG}
      ;;
		h )
			help
			exit 1
			;;
		\? )
			echo "Invalid option $opt"
			help
			exit 1
			;;
	esac
done

PACKAGE_VERSION=$VERSION
export PACKAGE_VERSION
export BUILD_TYPE

echo -e "BUILD_TYPE: ${BUILD_TYPE}"
echo -e "VER: ${PACKAGE_VERSION}"
echo -e "SRC_PATH: ${SRC_PATH}"
echo -e "CCACHE_DIR: ${CCACHE_DIR}"
echo -e "CCACHE_TMPDIR: ${CCACHE_TMPDIR}\n"

echo -e "Build ${BUILD_TYPE} version ${PACKAGE_VERSION} at ${SRC_PATH} as ${USER}@${HOSTNAME}"

if [ ! -z "$DEBUG" ]; then
  exit 0
fi

cd "${SRC_PATH}"
time make -j "$(nproc)"

exit 0

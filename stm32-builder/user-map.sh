#!/bin/bash

PROFILE_PATH_SCRIPT="/etc/profile.d/path.sh"

cat << EOF > $PROFILE_PATH_SCRIPT
#!/bin/sh
PATH=$PATH
EOF

# Running as a Docker
echo "export IS_DOCKER=1" | sudo tee -a /etc/profile 1>/dev/null

if [ -z "$USER_ID" ]; then
  echo "We need USER to be set!"; exit 1
fi

echo "Default: $USER_ID uid: $(id -u $USER_ID) gid: $(id -g $USER_ID)"

if [ ! -z "$HOST_UID" ]; then
  sudo usermod -u $HOST_UID $USER_ID
fi

if [ ! -z "$HOST_GID" ]; then
  sudo groupmod -g $HOST_GID $USER_ID
fi

id $USER_ID

HOME_PATH="/home/${USER_ID}"
#export USE_CCACHE=1
#export CCACHE_DIR="${HOME_PATH}/ccache"
#export CCACHE_TMPDIR="/tmp"

echo "BUILD: $BUILD BUILD_TAG: $BUILD_TAG BUILD_CONF: $BUILD_CONF"

sudo chown -R $(id -u $USER_ID).$(id -u $USER_ID) ${HOME_PATH}
#sudo chown $(id -u $USER_ID).$(id -u $USER_ID) ${CCACHE_DIR}

#echo "export USE_CCACHE=1" | sudo tee -a ${HOME_PATH}/.bashrc 1>/dev/null
#echo "export CCACHE_DIR=${HOME_PATH}/ccache" | sudo tee -a ${HOME_PATH}/.bashrc 1>/dev/null
#echo "export CCACHE_TMPDIR=/tmp" | sudo tee -a ${HOME_PATH}/.bashrc 1>/dev/null

# ssh key permission
if [ -d "${HOME_PATH}/.ssh" ]; then
  sudo chown -R $(id -u $USER_ID).$(id -u $USER_ID) ${HOME_PATH}/.ssh
  sudo chmod go-rwx ${HOME_PATH}/.ssh
  sudo chmod go+r ${HOME_PATH}/.ssh/known_hosts
  sudo chmod go-rwx ${HOME_PATH}/.ssh/id_rsa
fi

if [ ! -z "$BUILD" ]; then
	CMD="/usr/bin/build.sh $BUILD_TAG $BUILD_CONF"
	echo "CMD: $CMD"
	exec su - $USER_ID -c "$CMD"
else
	# if BUILD is not setted, execute bash.
	echo "Welcome to STM32 Build Docker!!!"
	sudo login -f "$USER_ID"
fi
echo "Good Bye. STM32 Build Docker!!!"
